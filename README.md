# ENTITY REFERENCES MAP

This project provides a dynamic entity references map based on jHTree jQuery
library.
It's very helpful on complicate references structure to see which entities are
referenced in the fields. Reverse references are also allowed.

For a full description of the module, visit the
[project page](https://www.drupal.org/sandbox/dmytro-aragorn/3259790).

To submit bug reports and feature suggestions, or track changes visit
[project issues](https://www.drupal.org/project/issues/3259790).

jHTree jQuery [library](https://github.com/naadydev/jHTree).


## REQUIREMENTS

This module requires no modules outside of Drupal core.


## INSTALLATION

Install as you would normally install a contributed Drupal module. For further
information, see [Installing Drupal Modules](https://www.drupal.org/docs/extending-drupal/installing-drupal-modules).


## CONFIGURATION

1. Configure the user permissions in `Administration » People » Permissions`:
  - %Content type%: View map
    Grants access for viewing the content type's map.
  - View any node map page
    Grants access for viewing the node map pages of all node types.

2. Configure entity types  in `Administration » Configuration » Content authoring
» Entity References Map`. Set entity types that will be excluded from mapping.

3. Manage reference map at `admin/structure/types/manage/%content_type%/map`:
  - enable mapping for content type;
  - enable mapping for the entity references field;
  - set header and content region colors;
  - configure nested levels you want to build:
    - choose excluded entity types;
    - set number of nested levels being shown;
    - set header and content region colors.


## MAINTAINERS

* Dmytro Sluchevskyi - [dmytro-aragorn](https://www.drupal.org/u/dmytro-aragorn)
* Pavlo Radyvonik - [paulrad](https://www.drupal.org/u/paulrad)

### This project is sponsored by:

* [DevBranch](https://www.drupal.org/devbranch)
* [Linnovate](https://www.drupal.org/linnovate)
