<?php

namespace Drupal\entity_references_map\Form;

use Drupal\Component\Utility\NestedArray;
use Drupal\Core\Cache\Cache;
use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\Core\Entity\ContentEntityTypeInterface;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\Form\FormStateInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Entity type disable configuration form.
 */
class SettingsForm extends ConfigFormBase {

  public const CONFIG_NAME = 'entity_references_map.config';

  /**
   * The EntityTypeManager.
   *
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  protected EntityTypeManagerInterface $entityTypeManager;

  /**
   * Constructs SettingsForm instance.
   */
  public function __construct(
    ConfigFactoryInterface $config_factory,
    EntityTypeManagerInterface $entityTypeManager
  ) {
    parent::__construct($config_factory);
    $this->entityTypeManager = $entityTypeManager;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container): SettingsForm|ConfigFormBase|static {
    return new static(
      $container->get('config.factory'),
      $container->get('entity_type.manager')
    );
  }

  /**
   * {@inheritdoc}
   */
  protected function getEditableConfigNames(): array {
    return [self::CONFIG_NAME];
  }

  /**
   * {@inheritdoc}
   */
  public function getFormId(): string {
    return 'entity_references_map.settings';
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state): array {

    $config_data = $this->config(self::CONFIG_NAME)->get();

    $entity_options_array = [];

    foreach ($this->entityTypesList() as $entity_type) {
      $entity_options_array[$entity_type->id()] = $entity_type->getLabel();
    }

    $form['entities'] = [
     '#type' => 'checkboxes',
      '#title' => $this->t('Excluded Entity Types from Entity References Map'),
      '#options' => $entity_options_array,
      '#description' => $this->t('Selected entity types will be excluded from mapping.'),
      '#default_value' => !empty($config_data['excluded_entity_types']) ? $config_data['excluded_entity_types'] : [],
    ];

    return parent::buildForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state): void {
    $values = $form_state->getValue('entities');
    $config = $this->config(self::CONFIG_NAME);
    foreach ($values as $key => $value) {
      if (!empty($value)) {
        $config->set("excluded_entity_types.$key", $value);
      }
      else {
        $config->clear("excluded_entity_types.$key");
      }
    }
    $config->save();
    parent::submitForm($form, $form_state);

    Cache::invalidateTags(['entity_references_map_exclude_list']);
  }

  /**
   * Returns list of entity types available on the site.
   */
  public function entityTypesList(): array {
    $entity_types = $this->entityTypeManager->getDefinitions();
    return array_filter($entity_types, static function ($entity_type) {
      return $entity_type instanceof ContentEntityTypeInterface;
    });
  }

}
