<?php

namespace Drupal\entity_references_map\Controller;

use Drupal\Component\Serialization\Json;
use Drupal\Component\Utility\Html;
use Drupal\Core\Access\AccessResult;
use Drupal\Core\Access\AccessResultInterface;
use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\Core\Controller\ControllerBase;
use Drupal\Core\Session\AccountInterface;
use Drupal\entity_references_map\EntityReferencesMapBuilderInterface;
use Drupal\entity_references_map\Form\MapForm;
use Drupal\node\NodeInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Map controller.
 */
class EntityReferencesMapController extends ControllerBase {

  /**
   * EntityReferencesMapBuilder service.
   *
   * @var \Drupal\entity_references_map\EntityReferencesMapBuilderInterface
   */
  protected EntityReferencesMapBuilderInterface $entityReferencesMapBuilder;

  /**
   * Config Factory.
   *
   * @var \Drupal\Core\Config\ConfigFactoryInterface
   */
  protected $configFactory;

  /**
   * Constructs EntityReferencesMapController object.
   *
   * @param \Drupal\entity_references_map\EntityReferencesMapBuilderInterface $entity_references_map_builder
   *   The Entity reference map builder.
   * @param \Drupal\Core\Config\ConfigFactoryInterface $config_factory
   *   The Config factory.
   */
  public function __construct(
    EntityReferencesMapBuilderInterface $entity_references_map_builder,
    ConfigFactoryInterface $config_factory,
  ) {
    $this->entityReferencesMapBuilder = $entity_references_map_builder;
    $this->configFactory = $config_factory;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container): EntityReferencesMapController|static {
    return new static(
      $container->get('entity_references_map.builder'),
      $container->get('config.factory'),
    );
  }

  /**
   * Builds a block for rendering map content.
   */
  public function mapOverview(NodeInterface $node) {
    $node_type = $node->getType();

    $tree = $this->entityReferencesMapBuilder->build($node);
    $tree_id = Html::getUniqueId('tree');

    return [
      'tree' => [
        '#type' => 'html_tag',
        '#tag' => 'div',
        '#attributes' => ['id' => $tree_id],
        '#attached' => [
          'library' => [
            'entity_references_map/jHTree',
          ],
          'drupalSettings' => [
            'entity_references_map' => [
              'jHTree' => Json::encode($tree),
            ],
          ],
        ],
        '#cache' => [
          'tags' => [
            'node:' . $node->id(),
            'config:node.type.' . $node_type,
            "config:core.entity_form_display.node.{$node_type}.default",
            'entity_references_map_exclude_list',
          ],
        ],
      ],
    ];
  }

  /**
   * Checks access for map page.
   *
   * @param \Drupal\Core\Session\AccountInterface $account
   *   The account.
   * @param \Drupal\node\NodeInterface $node
   *   A node object.
   *
   * @return \Drupal\Core\Access\AccessResultInterface
   *   The access result.
   */
  public function mapAccess(AccountInterface $account, NodeInterface $node): AccessResultInterface {
    $configs = $this->configFactory->get(MapForm::CONFIG_NAME)->get();
    return AccessResult::allowedIf(($account->hasPermission('view any node map page') ||
      $account->hasPermission("view node map page in {$node->getType()}"))
      && (!empty($configs[$node->bundle()]))
    );
  }

}
