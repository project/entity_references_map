<?php

namespace Drupal\entity_references_map;

use Drupal\Core\DependencyInjection\ContainerInjectionInterface;
use Drupal\Core\Entity\BundlePermissionHandlerTrait;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\StringTranslation\StringTranslationTrait;
use Drupal\node\Entity\NodeType;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Provides dynamic permissions for granting map-view access.
 *
 * @see entity_references_map.permissions.yml
 */
class EntityReferencesMapPermissions implements ContainerInjectionInterface {

  use BundlePermissionHandlerTrait;
  use StringTranslationTrait;

  /**
   * The entity type manager.
   *
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  protected $entityTypeManager;

  /**
   * Constructs a EntityReferencesMapPermissions instance.
   *
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entity_type_manager
   *   The EntityTypeManager.
   */
  public function __construct(EntityTypeManagerInterface $entity_type_manager) {
    $this->entityTypeManager = $entity_type_manager;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static($container->get('entity_type.manager'));
  }

  /**
   * Get permissions for node types.
   *
   * @return array
   *   Provide a list of permissions for node types.
   */
  public function getNodeTypesPermissions() {
    return $this->generatePermissions($this->entityTypeManager->getStorage('node_type')->loadMultiple(), [$this, 'buildPermissions']);
  }

  /**
   * Builds a list of content type permissions.
   *
   * @param \Drupal\node\Entity\NodeType $node_type
   *   The node type.
   *
   * @return array
   *   An array of permission names and descriptions.
   */
  protected function buildPermissions(NodeType $node_type) {
    $id = $node_type->id();
    $args = ['%content_type' => $node_type->label()];

    return [
      "view node map page in {$id}" => ['title' => $this->t('%content_type: View map', $args)],
    ];
  }
}
