<?php

namespace Drupal\entity_references_map;

use Drupal\Core\Entity\EntityInterface;
use Drupal\node\NodeInterface;

/**
 * Declares an interface for EntityReferencesMapBuilder.
 */
interface EntityReferencesMapBuilderInterface {

  /**
   * Builds node map.
   *
   * @param \Drupal\node\NodeInterface $node
   *   Node object.
   *
   * @return array
   *   Array of node map tree.
   */
  public function build(NodeInterface $node): array;

  /**
   * Create group element of the map tree.
   *
   * @param string $id
   *   Group id.
   * @param string $head
   *   Group head.
   * @param string $label
   *   Group label.
   * @param string $header_color
   *   The header background color.
   * @param string $content_color
   *   The content background color.
   *
   * @return array
   *   Structure for group element in the map tree.
   */
  public function createMapWrapper(string $id,
    string $head,
    string $label,
    string $header_color = '#cccccc',
    string $content_color = '#ffffff'): array;

  /**
   * @param \Drupal\Core\Entity\EntityInterface $entity
   *   The entity object.
   * @param \Drupal\Core\Entity\EntityInterface $parent_entity
   *   The parent entity object.
   * @param string $group
   *   The group.
   * @param   string $header_color
   *   The header background color.
   * @param   string $content_color
   *   The content background color.
   * @param bool $forward_reference
   *   The type of reference.
   *
   * @return array
   */
  public function createMapElement(EntityInterface $entity,
    EntityInterface $parent_entity,
    string $group,
    string $header_color,
    string $content_color,
    bool $forward_reference = TRUE): array;

  /**
   * Creates nested children arrays of referenced entities.
   *
   * @param \Drupal\Core\Entity\EntityInterface $entity
   *   The entity object.
   * @param array $excluded_entity_references
   *   List of disabled entity types for mapping.
   * @param string $header_color
   *   The header background color.
   * @param string $content_color
   *   The content background color.
   * @param int $max_depth
   *   Maximum depth of the referenced entities on the map.
   * @param int $current_depth
   *   Depth level counter.
   *
   * @return array
   *   Nested children array of referenced entities.
   */
  public function createChildElement(EntityInterface $entity,
    array $excluded_entity_references,
    string $header_color,
    string $content_color,
    int $max_depth,
    int $current_depth): array;

}
