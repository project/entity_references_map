/**
 * @file
 * Provides base JS for program map.
 */
(function (Drupal, $, once) {

  "use strict";

  Drupal.behaviors.mapjHTree = {
    attach: function (context, drupalSettings) {

      $(once('mapjHTree', '#tree', context)).each(function () {
        var $tree = $(this);

        $tree.jHTree({
          callType: 'obj',
          structureObj: [jQuery.parseJSON(drupalSettings.entity_references_map.jHTree)],
        });

        // Wait when the whole tree will be generated.
        var loadedTree = setInterval(function () {
          if ($tree.length) {
            // Calculate amount of children elements for each tree group.
            $tree.find('.after .funcbtna').each(function () {
              var nextChildren = $(this).closest('li.ui-droppable').find('> .trchl > ul > li').length;
              var allChildren = $(this).closest('li.ui-droppable').find('> .trchl li').length;

              $(this).prepend(`<span class="children-amount">${nextChildren} (${allChildren})</span>`);
            });

            // Prevent long line below the element if it has only one child.
            $tree.find('li.ui-droppable').each(function () {
              if ($(this).find('> .trchl > ul > li').length === 1) {
                $(this).find('> .trchl > ul').css('padding', 0);
                $(this).find('> .trchl > ul > .bfrul').hide();
              }
            });

            // Expand list of all external programs by default.
            $tree.find('li[id^="other-ext-program-"]').each(function () {
              $(this).find('> .after .funcbtna').show().click();
            });


            clearInterval(loadedTree);
          }
        }, 100);
      });
    }
  };

}(Drupal, jQuery, once));
